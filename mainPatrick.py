import twitterMiner as tm
import networkx as nx
import itertools
import traceback
import pickle
import shelve
import time
import sys
import os
from pprint import pprint
from time import sleep

spain = pickle.load(open('spain_detailed.p'))
england = pickle.load(open('england_detailed.p'))

twitter = tm.get_twitter()

def get_player_friends(team):
    g = nx.DiGraph()
    for player in team:
        player = tm.strip_none_values(player)
        screenName = player['screen_name']
        g.add_node(screenName, player=True, **player) 
        playerFriends = twitter.get_friends_list(screen_name=screenName)
        for friend in playerFriends['users']:
            friend = tm.strip_none_values(friend)
            g.add_node(friend['screen_name'], **friend)
            g.add_edge(screenName, friend['screen_name'])
    return g

def get_bulk_friends(team, team_name):
    ''' Getting EXTREEEME '''
    team_list = shelve.open('{}_friendIDs'.format(team_name))
    requests = 0
    try:
        for player in team:
            screenName = player['screen_name']
            if screenName in team_list:
                continue
            if requests >= 15:
                time.sleep(900)
                requests = 0
            player_id = player['id']
            playerFriends = twitter.get_friends_ids(user_id=player_id, count=5000)
            sleep(3)
            team_list[screenName] = playerFriends['ids']
            requests += 1
    except:
        print(traceback.format_exc())
    finally:
        print('Closing list')
        team_list.close()

def chunker(seq, size):
   return (seq[pos:pos + size] for pos in xrange(0, len(seq), size))

def process_bulk_friends(team_name):
    try:
        players = shelve.open('{}_friendIDs'.format(team_name), flag='r')
        requests = 0
        for player in players:
            playerids = players[player]
            processed = shelve.open('{}_proc/{}'.format(team_name, player))
            if player in processed:
                processed.close()
                continue
            friends = []
            for i in xrange(0, len(playerids),100):
                ids = playerids[i:i+100]
                if requests >= 60:
                    print('Sleeping...')
                    sleep(900)
                    requests = 0
                useridparm = ','.join(str(x) for x in ids)
                print('Processing {}'.format(useridparm))
                friends.append(twitter.lookup_user(user_id=useridparm))
                sleep(2)
                requests += 1
            processed[player] = friends
            processed.close()
    except:
        print(traceback.format_exc())
    finally:
        print('Closing lists')
        players.close()

def graph_bulk_friends(team, team_name):
    g = nx.DiGraph()
    for player in team:
        player = tm.strip_none_values(player)
        screenName  = player['screen_name']
        filename = '{}_proc/{}.db'.format(team_name, screenName)
        if os.path.isfile(filename):
            g.add_node(screenName, player=True, **player)
            friends = shelve.open(filename, flag='r')
            combined = list(itertools.chain.from_iterable(friends[screenName]))
            for friend in combined:
                friend = tm.strip_none_values(friend)
                g.add_node(friend['screen_name'], **friend)
                g.add_edge(screenName, friend['screen_name'])
            friends.close()
    return g

# Enables continuous tweet nabbing
def get_tweets(twitter, playerId, count=200, minId=sys.maxint):
    # Allows for resuming from previous tweet ID
    args = {'user_id':playerId, 'count':count, 'exclude_replies':True}
    if minId != sys.maxint:
        args['max_id'] = minId
    tweets = twitter.get_user_timeline(**args)
    for tweet in tweets:
        tweetId = tweet['id']
        if tweetId < minId:
            minId = tweetId 
    return (minId, tweets)

# Bipartite between the players and their tweets/retweets. 
# bipartite=0 for players, 1 for tweets/retweets
def get_player_tweets(team, network=None):
    search_status = shelve.open('search_status')
    lastId = None
    if network is None:
        g = nx.Graph()
    else:
        g = network

    count = 0
    try:
        for player in team:
            player = tm.strip_none_values(player)
            playerId = player['id']
            if playerId in g.node:
                continue #Already done this player!
            screenName = player['screen_name']
            g.add_node(playerId, bipartite=0, **player)
            # I could use lastId to do disaster recovery, but there's only
            # two requests needed for 400 tweets... I can redo that and not feel bad.
            lastId, playerTweets  = get_tweets(twitter, playerId)
            lastId, playerTweets2 = get_tweets(twitter, playerId, minId=lastId)
            lastId, playerTweets3 = get_tweets(twitter, playerId, minId=lastId)
            count += 3
            playerTweets += playerTweets2
            playerTweets += playerTweets3
            for tweet in playerTweets:
                tweet_id           = tweet['id']
                author_id          = tweet['user']['id']
                author_screen_name = tweet['user']['screen_name']
                tweet = tm.strip_none_values(tweet)
                g.add_node(tweet_id, author_id=author_id,
                        author_screen_name=author_screen_name,
                        bipartite=1,
                        **tweet)
                g.add_edge(playerId, tweet_id)
            if count % 300 == 0: # I feel clever for this one
                sleep(900) #Sleep 15 minutes (twitter window)
        return g
    except:
        print('OOPS! Check for CRASHDUMP.graphml.gz !!')
        print(traceback.format_exc())
    finally:
        nx.write_graphml(g, 'CRASHDUMP.graphml.gz')

#englandTweets = get_player_tweets(england)
#spainTweets   = get_player_tweets(spain)
#nx.write_graphml(englandTweets, 'englandtweets.graphml.gz')
#nx.write_graphml(spainTweets, 'spaintweets.graphml.gz')

#spainGraph = get_player_friends(spain)
#englandGraph = get_player_friends(england)
#nx.write_graphml(spainGraph, 'spain.graphml.gz')
#nx.write_graphml(englandGraph, 'england.graphml.gz')

#spainIds = get_bulk_friends(spain, 'spain')
#englandIds = get_bulk_friends(england, 'england')
#process_bulk_friends('spain')
#process_bulk_friends('england')
#nx.write_graphml(graph_bulk_friends(spain, 'spain'), 'spainV2.graphml.gz')
nx.write_graphml(graph_bulk_friends(england, 'england'), 'englandV2.graphml.gz')



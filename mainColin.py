import twitterMiner as tm
from pprint import pprint
import networkx as nx
#import matplotlib as plt

#get first 100 followers of David Beckham
twitter = tm.get_twitter()

followers = tm.getFollowers("DavidBeckhamWeb",twitter)['users']
followers2 = tm.getFollowers("CasillasWorld",twitter)['users']
folls  = range(len(followers))
folls2 = range(len(followers2))

for u in range(0,len(followers)):
    folls[u] = (followers[u]['screen_name'],followers[u]['lang'])
for w in range(0,len(followers2)):
    folls2[w] = (followers2[w]['screen_name'],followers2[w]['lang'])    

G = nx.Graph()
G.add_node('DavidBeckhamWeb', lang='en', starState = "En")
for u in folls:
    G.add_node(u[0], lang = u[1][:2], starState = "En")
    G.add_edge(u[0],'DavidBeckhamWeb')
for n in G.nodes():
    for m in G.nodes():
        if(G.node[n]['lang'] == G.node[m]['lang']):
           G.add_edge(n,m)

    
#H = nx.Graph()
#H.add_node('CasillasWorld', lang='es', starState = "Es")
H = nx.Graph()
H.add_node('CasillasWorld', lang='es', starState = "Es")
for u in folls2:
    H.add_node(u[0], lang = u[1][:2], starState = "Es")
    H.add_edge(u[0],'CasillasWorld')
for n in H.nodes():
    for m in H.nodes():
        if(H.node[n]['lang'] == H.node[m]['lang']):
           H.add_edge(n,m)


nx.write_graphml(G, 'g.graphml')
nx.write_graphml(H, 'h.graphml')

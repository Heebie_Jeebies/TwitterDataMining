#-*- coding: utf-8 -*- 
import twitterMiner as tm
import networkx as nx
import pickle
from pprint import pprint

spain = pickle.load(open('spain_detailed.p'))
england = pickle.load(open('england_detailed.p'))

twitter = tm.get_twitter()

def get_player_friends(g):
    gs = nx.read_graphml(g)
    for node in gs.nodes():
        screenName = gs.node[node] 
        playerFriends = twitter.get_friends_list(screen_name=screenName)
        for friend in playerFriends['users']:
            print(friend.keys()[0])
    return gs

spainGraph = get_player_friends('followersSpain.graphml')
#englandGraph = get_player_friends(england)

nx.write_graphml(spainGraph, 'spain528.graphml.gz')
#nx.write_graphml(englandGraph, 'england.graphml.gz')

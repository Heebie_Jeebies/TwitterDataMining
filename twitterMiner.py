# -*- coding: utf-8 -*- 
import ConfigParser
import time
import sys
from twython import Twython
from pprint import pprint
import networkx as nx


config = ConfigParser.ConfigParser()
config.read('oauth.config')

spain_players = {
    'Goalkeepers': [u'David de Gea', u'Víctor Valdés', u'Pepe Reina', u'Iker Casillas'],
    'Defenders': [u'Álvaro Arbeloa',u'César Azpilicueta',u'Raúl Albiol',u'Alberto Moreno',u'Iñigo Martínez',u'Jordi Alba',u'Nacho Monreal',u'Sergio Ramos',u'Gerard Piqué'],
    'Midfielders': [u'Santi Cazorla',u'Cesc Fàbregas',u'Jesús Navas',u'Mario Suárez',u'Javi García',u'Javi Martínez',u'Sergio Busquets',u'Andrés Iniesta',u'David Silva',u'Xavi Hernández',u'Xabi Alonso'],
    'Forwards': [u'Roberto Soldado',u'Fernando Torres',u'Pedro Rodríguez',u'David Villa',u'Álvaro Negredo',u'Juan Mata']
}

england_players = {
    'Goalkeepers': [u'Ben Foster',u'Fraser Forster',u'Jack Butland',u'Joe Hart',u'John Ruddy'],
    'Defenders': [u'Leighton Baines',u'Ryan Bertrand',u'Gary Cahill',u'Ryan Shawcross',u'Kyle Walker',u'Ashley Cole',u'Chris Smalling',u'Phil Jones',u'Kieran Gibbs',u'Steven Taylor',u'Steven Caulker',u'Glen Johnson',u'Joleon Lescott',u'Phil Jagielka',u'John Terry'],
    'Midfielders': [u'Frank Lampard',u'Steven Gerrard',u'James Milner',u'Michael Carrick',u'Jake Livermore',u'Jack Wilshere',u'Leon Osman',u'Andros Townsend',u'Ross Barkley',u'Jonjo Shelvey',u'Scott Parker',u'Ashley Young',u'Adam Johnson',u'Aaron Lennon',u'Raheem Sterling',u'Alex Oxlade-Chamberlain',u'Tom Cleverley',u'Adam Lallana'],
    'Forwards': [u'Theo Walcott',u'Danny Welbeck',u'Daniel Sturridge',u'Wayne Rooney',u'Andy Carroll',u'Rickie Lambert',u'Jermain Defoe']
}

def get_access_token():
    oauth = Twython(config.get('OAuth','APP_KEY'), config.get('OAuth','APP_SECRET'), oauth_version=2)
    config.set('OAuth', 'ACCESS_TOKEN', oauth.obtain_access_token())
    config_file = open('oauth.config', 'w')
    config.write(config_file)
    config_file.close()

def get_twitter(context='application'):
    if context == 'application':
        if not config.has_option('OAuth', 'ACCESS_TOKEN'):
            get_access_token()
        APP_KEY = config.get('OAuth', 'APP_KEY')
        ACCESS_TOKEN = config.get('OAuth', 'ACCESS_TOKEN')
        return Twython(APP_KEY, access_token=ACCESS_TOKEN)
    elif context == 'user':
        APP_KEY = config.get('OAuth', 'APP_KEY')
        APP_SECRET = config.get('OAuth', 'APP_SECRET')
        twitter = Twython(APP_KEY, APP_SECRET)
        pass
    else:
        return

def get_verified_users(twitter, search):
    if not isinstance(search, list):
        return
    ret = {}
    for person in search:
        results = twitter.search_users(q=person)
        ret[person] = [(player['name'],player['screen_name'],player['description']) for player in results if player['verified']]
    return ret

def repeat(function, args, inrequests=0, sleeppoint=30,time=900, maxIterations=sys.maxint):
    results = []
    counter = 0
    curs    = -1
    requests = inrequests 
    while counter < maxIterations and curs != 0:
        if requests >= sleeppoint:
            time.sleep(time)
            requests = 0
        res = function(cursor=curs, **args)
        results.append(res)
        curs = res['next_cursor']
        requests += 1
    return results


def strip_none_values(inputdict):
    return dict((k, v) for k, v in inputdict.iteritems() if v is not None and type(v) is not dict)

def get_team_spain(**params):
    return get_team(spain_players, **params)

def get_team_england(**params):
    return get_team(england_players, **params)

def get_team(team, squads=False):
    if squads:
        return team
    else:
        ret = []
        for squad,players in team.iteritems():
            ret += players
        return ret

def getFollowers(user,twitter):
    #retrieve the followers of a user and put them in a list and return that list
    
    followers = twitter.get_followers_list(screen_name =user,count=100 )
    return followers

def mutualFollows(user):
    #put code here to return a list of followers also followed by user. Relationship is non-directional, mutual
    return null
def getCountry(user):
    #put code here that gets home country info for user and returns it as a touple
    return null
def getLang(user):
    #return the language of the user
    return null
def getRetwCount(user, player):
    #returns how many times user has retweeted player for the last 150 tweets from player.
    #Perhaps this can reveal how much of a fan certain followers are.
    return null

def getFollsTeam(players, twer):
    g = nx.Graph()
    #get 150 followers from each player on a team and make a dictionary of followers
    folls = dict()
    for p in players:
        folls[p] =  getFollowers(p,twer)['users']
        for f in folls[p]:
            print(f['screen_name'])
            g.add_node(f['screen_name'],player = p)
        #print(g.nodes(data=True))
##            for c in g.nodes():
##                for l in g.nodes():
##                    if(c['player']==l['player']):
##                        g.add_edge(l,c)

        nx.write_graphml(g, "followersEng.graphml")
        time.sleep(15)
    return folls

